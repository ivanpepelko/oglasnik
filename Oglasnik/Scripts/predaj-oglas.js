﻿$(function () {
    $('#tip_oglasa').on('change', function () {
        $('#loading').show()
        var id = $(this).find('option:selected').val()
        var $detalji = $('#detalji')

        switch (id) {
            case 'graficke':
                $detalji.load('GrafickaAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'maticne':
                $detalji.load('MaticnaAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'memorije':
                $detalji.load('MemorijaAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'psu':
                $detalji.load('PSUAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'pohrana':
                $detalji.load('PohranaAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'procesori':
                $detalji.load('ProcesorAjax', null, function () {
                    $('#loading').hide()
                })
                break
            case 'null':
            default:
                $('#loading').hide()
                $detalji.html(null)
                break
        }

    })
    
    $('.body-content form').on('submit', function (e) {
        var fields = $(this).serializeObject()

        var formIsValid = 
            fields.cijena !== "" &&
            fields.naslov !== "" &&
            fields.opis !== "" &&
            fields.tip !== "null"

        console.log(fields)
    })
    
})
