﻿(function ($) {
    $.fn.serializeObject = function () {
        $object = $(this)

        if ($object.is('form') || $object.is(':input')) {
            $formFields = $object.serialize().split('&')
            var fields = {}

            $formFields.forEach(function (fld) {
                var fvals = fld.split('=')
                fields[fvals[0]] = fvals[1]
            })

            return fields
        } else return {}
    }
}(jQuery))

$(function () {

    $('#oglasi-typeahead').typeahead({
        highlight: true
    },
    {
        name: 'oglasi',
        display: 'naslov',
        source: new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '/Oglasi/Search?q=%QUERY%',
                wildcard: '%QUERY%'
            }
        })
    }).on('typeahead:select', function (e, data) {
        window.location = '/Oglasi/Details/' + data.Id
    })

})
