﻿using Microsoft.AspNet.Identity;
using Oglasnik.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SelectPdf;
using System.Reflection;

namespace Oglasnik.Controllers {
    public class OglasiController : Controller {
        private OglasnikDbContext db = new OglasnikDbContext();

        // GET: Oglasi
        public ActionResult Index() {
            return Redirect("/");
            return View(db.Oglasi.ToList());
        }

        // GET: Oglasi/Details/5
        public ActionResult Details(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Oglasi oglas = db.Oglasi.Find(id);
            if (oglas == null) {
                return HttpNotFound();
            }

            KomponentaBase komponenta;
            switch (oglas.tip_oglasa) {
                case "graficke":
                    komponenta = db.Graficke.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "maticne":
                    komponenta = db.Maticne.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "memorije":
                    komponenta = db.Memorije.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "psu":
                    komponenta = db.Psu.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "pohrana":
                    komponenta = db.Pohrana.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "procesori":
                    komponenta = db.Procesori.Single(k => k.oglas_id == oglas.Id);
                    break;
                default:
                    throw new Exception("This should never happen!");
            }

            ApplicationUser oglasivac;
            using (ApplicationDbContext appdb = new ApplicationDbContext()) {
                oglasivac = appdb.Users.Find(oglas.korisnik_id);
            }

            ViewBag.Komponenta = komponenta;
            ViewBag.Oglasivac = oglasivac;
            return View(oglas);
        }

		/// <summary>
		/// Akcija koja vraća PDF
		/// </summary>
		/// <returns>pdf file</returns>
		/// <param name="id">id oglasa</param>
        [HttpGet]
        public ActionResult OglasPdf(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Oglasi oglas = db.Oglasi.Find(id);
            if (oglas == null) {
                return HttpNotFound();
            }
            
            KomponentaBase komponenta;
            switch (oglas.tip_oglasa) {
                case "graficke":
                    komponenta = db.Graficke.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "maticne":
                    komponenta = db.Maticne.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "memorije":
                    komponenta = db.Memorije.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "psu":
                    komponenta = db.Psu.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "pohrana":
                    komponenta = db.Pohrana.Single(k => k.oglas_id == oglas.Id);
                    break;
                case "procesori":
                    komponenta = db.Procesori.Single(k => k.oglas_id == oglas.Id);
                    break;
                default:
                    throw new Exception("This should never happen!");
            }

            ApplicationUser oglasivac;
            using (ApplicationDbContext appdb = new ApplicationDbContext()) {
                oglasivac = appdb.Users.Find(oglas.korisnik_id);
            }

            PdfDocument doc = new PdfDocument();
            PdfFont font = doc.AddFont(SelectPdf.PdfStandardFont.Helvetica);
            font.Size = 20;
            PdfPage page = doc.AddPage();

            string html = "<style>*{font-family:sans-serif;} li{margin-bottom:5px;font-size:1.2em;} h1{font-size:1.5em;}</style>";
            html += string.Format("<h1>{0}</h1><hr />", oglas.naslov);
            html += string.Format("<ul><li><strong>Opis:</strong><br />{0}</li>", oglas.opis.Replace("\r\n", "<br>"));

            PropertyInfo[] props = komponenta.GetType().GetProperties();
            Array.Reverse(props);
            foreach (var prop in props) {
                if ((new string[] { "Id", "oglas_id", "oglas" }).Contains(prop.Name)) {
                    continue;
                }
                html += string.Format("<li><strong>{0}:</strong> {1}</li>",
                    prop.CustomAttributes.ElementAt(0).NamedArguments[0].TypedValue.ToString().Replace("\"", ""),
                    komponenta.GetType().GetProperty(prop.Name).GetValue(komponenta));
            }
            html += string.Format("<li><strong>Objavio korisnik:</strong> {0}</li><li><strong>Tel:</strong> {1}</li><li><strong>Cijena:</strong> {2} KN</li><li><strong>Datum:</strong> {3}</li></ul>",
                oglasivac.UserName,
                oglasivac.PhoneNumber,
                oglas.cijena,
                oglas.datum_objave
                );

            PdfHtmlElement content = new PdfHtmlElement(html, "localhost");
            
            page.Add(content);
            return File(doc.Save(), "application/pdf", string.Format("oglas {0}.pdf", oglas.naslov));
        }

        // GET: Oglasi/Create
        [Authorize]
        public ActionResult Create() {
            return View();
        }

        // POST: Oglasi/Create
		/// <summary>
		/// Kreiranje oglasa - svaki oglas sprema se u 2 tablice u bazi:
		/// oglasi (zaglavlje) i ovisno o komponenti u zasebnu tablicu.
		/// </summary>
		/// <param name="oglasi">Oglasi.</param>
		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize]
        public ActionResult Create([Bind(Include = "Id,naslov,opis,cijena,tip_oglasa")] Oglasi oglasi) {
            if (ModelState.IsValid) {

                oglasi.datum_objave = DateTime.Now;
                oglasi.korisnik_id = User.Identity.GetUserId();
                db.Oglasi.Add(oglasi);
                db.SaveChanges();

                //"ručno" bindanje, switch generiran php skriptom u folderu
                using (OglasnikDbContext dbc = new OglasnikDbContext()) {
                    string tip = oglasi.tip_oglasa;

                    switch (tip.ToLower()) {
                        case "graficke":
                            Graficke gr = new Graficke() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                memorija = Request.Params.Get("memorija"),
                                slot = Request.Params.Get("slot"),
                                frek_memorije = Request.Params.Get("frek_memorije"),
                                frek_jezgre = Request.Params.Get("frek_jezgre"),
                                potrosnja = Request.Params.Get("potrosnja"),
                            };
                            dbc.Graficke.Add(gr);
                            break;
                        case "maticne":
                            Maticne ma = new Maticne() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                socket = Request.Params.Get("socket"),
                                potrosnja = Request.Params.Get("potrosnja"),
                                memorija = Request.Params.Get("memorija"),
                            };
                            dbc.Maticne.Add(ma);
                            break;
                        case "memorije":
                            Memorije me = new Memorije() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                tip = Request.Params.Get("tip"),
                                frekvencija = Request.Params.Get("frekvencija"),
                                velicina = Request.Params.Get("velicina"),
                            };
                            dbc.Memorije.Add(me);
                            break;
                        case "pohrana":
                            Pohrana po = new Pohrana() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                rpm = Request.Params.Get("rpm"),
                                sata_ver = Request.Params.Get("sata_ver"),
                                velicina = Request.Params.Get("velicina"),
                                brzina_cit = Request.Params.Get("brzina_cit"),
                                brzina_pis = Request.Params.Get("brzina_pis"),
                            };
                            dbc.Pohrana.Add(po);
                            break;
                        case "procesori":
                            Procesori pr = new Procesori() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                jezgre = Request.Params.Get("jezgre"),
                                socket = Request.Params.Get("socket"),
                                frekvencija = Request.Params.Get("frekvencija"),
                                potrosnja = Request.Params.Get("potrosnja"),
                            };
                            dbc.Procesori.Add(pr);
                            break;
                        case "psu":
                            Psu ps = new Psu() {
                                oglas_id = oglasi.Id,
                                proizvodac = Request.Params.Get("proizvodac"),
                                model = Request.Params.Get("model"),
                                snaga = Request.Params.Get("snaga"),
                            };
                            dbc.Psu.Add(ps);
                            break;
                    }

                    dbc.SaveChanges();

                }

                return RedirectToAction("Index", "Home");
            }

            return View(oglasi);
        }

        // GET: Oglasi/Edit/5
        [Authorize]
        public ActionResult Edit(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oglasi oglasi = db.Oglasi.Find(id);
            if (oglasi == null) {
                return HttpNotFound();
            }
            return View(oglasi);
        }

        // POST: Oglasi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,naslov,opis")] Oglasi oglasi) {
            if (ModelState.IsValid) {
                db.Entry(oglasi).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oglasi);
        }

        // GET: Oglasi/Delete/5
        [Authorize]
        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oglasi oglasi = db.Oglasi.Find(id);
            if (oglasi == null) {
                return HttpNotFound();
            }
            return View(oglasi);
        }

        // POST: Oglasi/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            Oglasi oglasi = db.Oglasi.Find(id);
            db.Oglasi.Remove(oglasi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

		/// <summary>
		/// Metoda za dohvaćanje JSON-a za search.
		/// </summary>
		/// <param name="q">Query</param>
        public JsonResult Search(string q) {
			return Json (db.Oglasi.Where (og => og.naslov.Contains (q)), JsonRequestBehavior.AllowGet);
		}

		#region ajax_forme
		//dijelovi formi specifični za komponentu
        public ActionResult GrafickaAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult MaticnaAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }
        public ActionResult MemorijaAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult PSUAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult PohranaAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }
        public ActionResult ProcesorAjax(int? id) {
            if (!Request.IsAjaxRequest())
                return RedirectToAction("Index", "Home");
            return View();
        }
		#endregion

		#region pojedinacni_oglas
        [HttpGet]
        public ActionResult Graficke() {
            return View(db.Graficke);
        }

        [HttpGet]
        public ActionResult Maticne() {
            return View(db.Maticne);
        }

        [HttpGet]
        public ActionResult Memorije() {
            return View(db.Memorije);
        }

        [HttpGet]
        public ActionResult Pohrana() {
            return View(db.Pohrana);
        }

        [HttpGet]
        public ActionResult Procesori() {
            return View(db.Procesori);
        }

        [HttpGet]
        public ActionResult Psu() {
            return View(db.Psu);
        }
		#endregion

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
