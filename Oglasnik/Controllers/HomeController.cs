﻿using Oglasnik.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Oglasnik.Controllers {
    public class HomeController : Controller {

        private OglasnikDbContext db = new OglasnikDbContext();

        public ActionResult Index() {
            return View(db.Oglasi);
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}