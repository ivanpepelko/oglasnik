<?php
//generator za switch u OglasiController.cs, metoda Create.

//"I have only one purpose."
$my = new mysqli('localhost', 'root', '', 'information_schema');

if ($my->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli->connect_error . PHP_EOL;
}

$res = $my->query("SELECT TABLE_NAME as 'table', COLUMN_NAME as 'name' FROM COLUMNS WHERE TABLE_SCHEMA='oglasnik'")->fetch_all(MYSQLI_ASSOC);

if (!$res) {
    echo "Failed to run query: (" . $my->errno . ") " . $my->error . PHP_EOL;
}

$my->close();

$tables = [];
foreach ($res as $row) {
    $tables[$row['table']][] = $row['name'];
}

//FUZZY WOOZIE
$generated = "switch (tip.ToLower()) {\n";
foreach ($tables as $table => $cols) {
    if ($table === "oglasi") {
        continue;
    }
    
    $uc_table = ucfirst($table);

    $generated .= <<<gen
        case "$table":
            $uc_table $table[0]$table[1] = new $uc_table() {
                oglas_id = oglasi.Id,
            
gen;

    foreach ($cols as $col) {
        if ($col === "id" || $col === "oglas_id") {
            continue;
        } else {
            $generated .= "\t\t$col = Request.Params.Get(\"$col\"),\n";
        }
    }

    $generated .= <<<gen
            };
            dbc.$uc_table.Add($table[0]$table[1]);
        break;
            
gen;
}
$generated .= "}\n";

echo "Everything went just fine!\n";
file_put_contents('switch.txt', $generated);
echo 'Saved into file switch.txt.';
