switch (tip.ToLower()) {
        case "graficke":
            Graficke gr = new Graficke() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		memorija = Request.Params.Get("memorija"),
		slot = Request.Params.Get("slot"),
		frek_memorije = Request.Params.Get("frek_memorije"),
		frek_jezgre = Request.Params.Get("frek_jezgre"),
		potrosnja = Request.Params.Get("potrosnja"),
            };
            dbc.Graficke.Add(gr);
        break;
                    case "maticne":
            Maticne ma = new Maticne() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		socket = Request.Params.Get("socket"),
		potrosnja = Request.Params.Get("potrosnja"),
		memorija = Request.Params.Get("memorija"),
            };
            dbc.Maticne.Add(ma);
        break;
                    case "memorije":
            Memorije me = new Memorije() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		tip = Request.Params.Get("tip"),
		frekvencija = Request.Params.Get("frekvencija"),
		velicina = Request.Params.Get("velicina"),
            };
            dbc.Memorije.Add(me);
        break;
                    case "pohrana":
            Pohrana po = new Pohrana() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		rpm = Request.Params.Get("rpm"),
		sata_ver = Request.Params.Get("sata_ver"),
		velicina = Request.Params.Get("velicina"),
		brzina_cit = Request.Params.Get("brzina_cit"),
		brzina_pis = Request.Params.Get("brzina_pis"),
            };
            dbc.Pohrana.Add(po);
        break;
                    case "procesori":
            Procesori pr = new Procesori() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		jezgre = Request.Params.Get("jezgre"),
		socket = Request.Params.Get("socket"),
		frekvencija = Request.Params.Get("frekvencija"),
		potrosnja = Request.Params.Get("potrosnja"),
            };
            dbc.Procesori.Add(pr);
        break;
                    case "psu":
            Psu ps = new Psu() {
                oglas_id = oglasi.Id,
            		proizvodac = Request.Params.Get("proizvodac"),
		model = Request.Params.Get("model"),
		snaga = Request.Params.Get("snaga"),
            };
            dbc.Psu.Add(ps);
        break;
            }
