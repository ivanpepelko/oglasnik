﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oglasnik.Models {
    public class OglasnikDbContext : DbContext {

        public OglasnikDbContext() :
            base("OglasnikDbContext") { }

        public DbSet<Oglasi> Oglasi { get; set; }

        public DbSet<Graficke> Graficke { get; set; }

        public DbSet<Maticne> Maticne { get; set; }

        public DbSet<Memorije> Memorije { get; set; }

        public DbSet<Pohrana> Pohrana { get; set; }

        public DbSet<Procesori> Procesori { get; set; }

        public DbSet<Psu> Psu { get; set; }

    }
}
