﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {

    [Table("pohrana")]
    public class Pohrana : KomponentaBase {
        
        [Display(Name = "Okretaja/min")]
        public string rpm { get; set; }

        [Display(Name = "Sata verzija")]
        public string sata_ver { get; set; }

        [Display(Name = "Veličina")]
        public string velicina { get; set; }

        [Display(Name = "Brzina čitanja")]
        public string brzina_cit { get; set; }

        [Display(Name = "Brzina pisanja")]
        public string brzina_pis { get; set; }

    }
}