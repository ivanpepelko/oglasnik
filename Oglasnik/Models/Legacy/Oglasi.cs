﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MySql.Data.Types;

namespace Oglasnik.Models {
    [Table("oglasi")]
    public class Oglasi {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name="Cijena")]
        public decimal cijena { get; set; }

        [Display(Name = "Naslov")]
        public string naslov { get; set; }

        [Display(Name = "Opis")]
        [DataType(DataType.MultilineText)]
        public string opis { get; set; }

        public DateTime datum_objave { get; set; }

        public string korisnik_id { get; set; }

        public string tip_oglasa { get; set; }

        [NotMapped]
        public ApplicationUser korisnik {
            get {
                using (ApplicationDbContext db = new ApplicationDbContext()) {
                    return db.Users.Find(korisnik_id);
                }
            }
        }
        
    }
}