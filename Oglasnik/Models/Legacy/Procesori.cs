﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {

    [Table("procesori")]
    public class Procesori : KomponentaBase {
                
        [Display(Name = "Broj jezgri")]
        public string jezgre { get; set; }

        [Display(Name = "Socket")]
        public string socket { get; set; }

        [Display(Name = "Frekvencija")]
        public string frekvencija { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

    }
}