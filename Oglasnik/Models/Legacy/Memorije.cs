﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {
    [Table("memorije")]
    public class Memorije : KomponentaBase {

        [Display(Name = "Tip")]
        public string tip { get; set; }

        [Display(Name = "Frekvencija")]
        public string frekvencija { get; set; }

        [Display(Name = "Veličina")]
        public string velicina { get; set; }

    }
}