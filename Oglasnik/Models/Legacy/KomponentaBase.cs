﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {
    public abstract class KomponentaBase {

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int oglas_id { get; set; }

        [Display(Name = "Proizvođač")]
        public string proizvodac { get; set; }

        [Display(Name = "Model")]
        public string model { get; set; }

        [NotMapped]
        public Oglasi oglas {
            get {
                using (OglasnikDbContext db = new OglasnikDbContext()) {
                    return db.Oglasi.Find(oglas_id);
                }
            }
        }
    }
}
