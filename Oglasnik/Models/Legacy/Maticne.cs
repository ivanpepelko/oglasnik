﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oglasnik.Models {

    [Table("maticne")]
    public class Maticne : KomponentaBase {

        [Display(Name = "Socket")]
        public string socket { get; set; }

        [Display(Name = "Potrošnja")]
        public string potrosnja { get; set; }

        [Display(Name = "Memorija")]
        public string memorija { get; set; }

    }
}